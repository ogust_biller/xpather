import click
from decimal import Decimal
import xml.etree.ElementTree as etree 
import elementpath
from pathlib import Path

def is_wild(s):
    return True if ('*' in s) or ('?' in s) else False

def process_file(filename, xpathexpr):
    tree = etree.parse(filename)
    root = tree.getroot()
    r = []
    try: 
        r = elementpath.select(root,xpathexpr)
    except elementpath.exceptions.ElementPathTypeError as e:
        print(e)
    ##print (elementpath)
    return r
    

@click.command()
@click.argument('xpathexpression')
@click.argument('xmlfile')
@click.option('--agregate', '-a', default = False, is_flag = True, required = False  )
def main(xpathexpression, xmlfile, agregate):
    """
        xpather is command line tool that calculate xpath expression by xml file(s)
        example: xpather "xpath_expression" inputdir/xmlfile.xml -a
                 or
                 xpather "xpath_expression" inputdir/*.xml -a
    """
    ##print(agregate)
    flist =  list(Path(xmlfile).parent.glob(Path(xmlfile).name))
    ##print(flist) 
    s = Decimal('0.00')
    if len(flist) > 0 :
        for f in flist:
            l = process_file(f, xpathexpression)
            if agregate and (len(l) > 0): s = s + Decimal(l)
            else: print(l)
        if agregate: print(s)
    else:
        click.echo('nothing to do')

if __name__=="__main__":
    main()

